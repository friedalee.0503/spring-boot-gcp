## spring-boot-gcp

Introduce how to build a spring boot project with docker, and deploy on k8s

# 1. Concept
  * `Docker Image` 與 `Container` 
  - `Docker Image` 程式跟環境包成一個 `image` ，是一個可以獨立執行的輕量級套件，其包含所有執行程式所需要的函式庫、環境變數與設定檔等
  - `Container`  容器（container）則是一個影像（image）的執行實體，就是將影像（image）載入至記憶體中執行之後的環境，他們之間的關係就像程式碼跟跑起來的程式，執行指令 `docker run` 把 `image` 跑起來變成 `container`
    <img src="img/docker.png" alt="docker" width="40%"/>

  * `k8s`運行機制  
    <img src="img/k8s.png" alt="k8s" width="80%"/>

  * `Dockerfile` 與 `docker-compose.yml`
  - `Dockerfile` 是建立 services container 的設定檔 
  - `docker-compose.yml` 有點像是執行多包 `Dockerfile` 的整合檔案，順序一定是由 build 開始再去執行其他指令， `image` 的選擇或環境參數可以直接先在 `Dockerfile` 設定好，執行指令 `docker-compose up` 啟動配置  
  
  
